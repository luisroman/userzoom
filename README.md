UserZoom test

By Luis Roman

Used technology for the tests:
- Kotlin
- Spring Boot 2
- Selenium 3
- Allure

With this framework you can execute your tests with the next browsers (Of course, is possible to add all the Browsers you want to test):
- Chrome version 74 (Windows, MAC and Linux)
- Firefox (Windows, MAC and Linux)
- Internet Explorer 11
- Edge version 18

Even you can execute the test in local or in a Grid

How to execute the tests:
- This framework is configured to execute the test in Chrome by default, but you can change this in 3 ways:
    - Going to the pom.xml file where you have all the profiles (local ones) and choose the one you want to execute
    - Going to the SeleniumTest class and add a @ActiveProfiles annotation with the profile you want to execute, all the profiles will be described above
    - Or executing by command: **mvn test** (this will execute all the tests with chrome, if you want to change the browser you have to put the next -Dspring.profiles.active=* desired browser *, for example: **-Dspring.profiles.active=Firefox**)
        - If you want to execute in a grid you have 2 ways to put the grid address:
            - Changing the variable in SeleniumSetUp called gridAddress
            - If you execute by command line you can add the option -DGRID_ADDRESS=http://* ip address and port *
    - An example would be: mvn test -Dspring.profiles.active=GridChrome -DGRID_ADDRESS=http://192.168.1.43:4444/wd/hub
- After all the tests has been executed you can generate a report with allure executing the command: **mvn allure:serve**
            
Profiles you can use:
- Chrome
- Firefox
- IE
- Edge
- GridChrome
- GridFirefox
- GridIE
- GridEdge

How this framework works:
- The first what this framework does is create a driver for the desired browser, you can see this in the SeleniumSetUp class
- After that, we inject the driver in all the Pages and Utils where is necessary to have
- And before executing the test, we inject the Pages with the driver injected to the tests so the tests can be easily executed
- How to define the web is gonna be opened for a test? Right now is in the application.yml file, this give us a chance to create multiple profiles for multiple environments (PROD, PRE, DEV, etc...) to be able to use the environment we need for our tests
- After each test is finished we are taking an screenshot to see the status of the tests
- When all the tests are finished you have the change to generate the allure report
