package com.luisroman.userzoom.behaviors.login

import com.luisroman.userzoom.page.login.LoginPage
import com.luisroman.userzoom.utils.SeleniumTest
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.junit.jupiter.SpringExtension

@SeleniumTest
@RunWith(JUnitPlatform::class)
@ExtendWith(SpringExtension::class)
@DisplayName("Contact Us Test")
open class ContactUsTest {

    @Autowired
    lateinit var loginPage: LoginPage

    @Test
    fun `Contact Us link is displayed with proper Href`() {
        assertTrue(loginPage.isContactUsLinkWithProperHrefDisplayed())
    }

}