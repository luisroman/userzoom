package com.luisroman.userzoom.page.main

import com.luisroman.userzoom.page.BasePage
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
open class ProjectDashboardPage: BasePage() {

    @PostConstruct
    open fun initProjectDashboard() = initializePageFactory(this)

    fun isProjectDashboardDisplayed(): Boolean = getProjectDashboardMain.isDisplayed

    @FindBy(id = PROJECT_DASHBOARD_ID)
    private lateinit var getProjectDashboardMain: WebElement

    companion object {
        private const val PROJECT_DASHBOARD_ID = "project-dashboard-main"
    }

}