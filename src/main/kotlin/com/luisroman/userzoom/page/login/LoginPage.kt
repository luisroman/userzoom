package com.luisroman.userzoom.page.login

import com.luisroman.userzoom.page.BasePage
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
open class LoginPage: BasePage() {

    @PostConstruct
    open fun initLogin() = initializePageFactory(this)

    fun clickSubmitButton() {
        elementUtils.waitForClickable(getSubmitButton)
        getSubmitButton.click()
    }

    fun fillEmailInput(email: String) {
        elementUtils.waitForClickable(getEmailInput)
        getEmailInput.sendKeys(email)
    }

    fun fillPasswordInput(password: String) {
        elementUtils.waitForClickable(getPasswordInput)
        getPasswordInput.sendKeys(password)
    }

    fun login(email: String, password: String) {
        fillEmailInput(email)
        fillPasswordInput(password)
        clickSubmitButton()
    }

    fun isEmailInvalid(): Boolean = driver.findElement(By.cssSelector("input[name='$EMAIL_NAME']:invalid")).isDisplayed

    fun isEmailRequiredAlertDisplayed(): Boolean = getEmailRequiredAlert.isDisplayed

    fun isPasswordRequiredAlertDisplayed(): Boolean = getPasswordRequiredAlert.isDisplayed

    fun isUserNotFoundAlertDisplayed(): Boolean {
        val userNotFound = getUserNotFoundAlert.findElement(By.xpath("//*/span/span[text()=\"$USER_NOT_FOUND_TEXT\"]"))
        elementUtils.waitForClickable(userNotFound)
        return userNotFound.isDisplayed
    }

    fun isUserBlockedAlertDisplayed(): Boolean {
        val userBlocked = getUserNotFoundAlert.findElement(By.xpath("//*/span/span[text()=\"$USER_BLOCKED_TEXT\"]"))
        elementUtils.waitForClickable(userBlocked)
        return userBlocked.isDisplayed
    }

    fun tryLoginXTimes(email: String, password: String, times: Int) {
        login(email, password)
        for (i in 1..times) {
            getUserNotFoundAlert.isDisplayed
            clickSubmitButton()
        }
    }

    fun clickForgotPasswordLink() {
        elementUtils.waitForClickable(getForgotPasswordLink)
        getForgotPasswordLink.click()
    }

    fun isContactUsLinkWithProperHrefDisplayed(): Boolean = getContactUsLink.isDisplayed


    fun openWebWithUnlockedAccountMessage() = driver.get("$mainUrl/?msg=S200")

    fun isUserUnlockedMessageDisplayed(): Boolean {
        elementUtils.waitForClickable(getUnlockedSuccesfulAlert)
        return getUnlockedSuccesfulAlert.isDisplayed
    }

    @FindBy(name = SUBMIT_NAME)
    private lateinit var getSubmitButton: WebElement

    @FindBy(name = EMAIL_NAME)
    private lateinit var getEmailInput: WebElement

    @FindBy(name = PASSWORD_NAME)
    private lateinit var getPasswordInput: WebElement

    @FindBy(id = EMAIL_REQUIRED_ID)
    private lateinit var getEmailRequiredAlert: WebElement

    @FindBy(id = PASSWORD_REQUIRED_ID)
    private lateinit var getPasswordRequiredAlert: WebElement

    @FindBy(className = USER_NOT_FOUND_CLASS)
    private lateinit var getUserNotFoundAlert: WebElement

    @FindBy(className = FORGOT_PASSWORD_CLASS)
    private lateinit var getForgotPasswordLink: WebElement

    @FindBy(xpath = "//a[@class='$CONTACT_US_CLASS' and @href='$CONTACT_US_HREF']")
    private lateinit var getContactUsLink: WebElement

    @FindBy(className = SUCCESS_ALERT_CLASS)
    protected lateinit var getSuccessAlert: WebElement

    @FindBy(xpath = "//*/span/span[text()=\"$USER_UNLOCKED_TEXT\"]")
    private lateinit var getUnlockedSuccesfulAlert: WebElement

    companion object {
        private const val SUBMIT_NAME = "submit"
        private const val EMAIL_NAME = "email"
        private const val PASSWORD_NAME = "password"
        private const val EMAIL_REQUIRED_ID = "auth0-lock-error-msg-email"
        private const val PASSWORD_REQUIRED_ID = "auth0-lock-error-msg-password"
        private const val USER_NOT_FOUND_CLASS = "auth0-global-message-error"
        private const val FORGOT_PASSWORD_CLASS = "auth0-lock-alternative-link"
        private const val CONTACT_US_CLASS = "contact-us__link"
        private const val CONTACT_US_HREF = "mailto:supportmn@userzoom.com"
        private const val USER_NOT_FOUND_TEXT = "We're sorry, this email/password combination could not be found."
        private const val USER_BLOCKED_TEXT = "Your profile has been blocked after multiple consecutive login attempts."
        private const val SUCCESS_ALERT_CLASS = "auth0-global-message-success"
        private const val USER_UNLOCKED_TEXT = "Your profile has been successfully unblocked. Please, log in again."
    }

}