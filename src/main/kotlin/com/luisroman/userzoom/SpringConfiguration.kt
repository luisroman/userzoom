package com.luisroman.userzoom

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
open class SpringConfiguration {

    companion object {
        fun main(args: Array<String>) {
            SpringApplication.run(SpringConfiguration::class.java, *args)
        }
    }
}